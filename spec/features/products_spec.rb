require 'rails_helper'

RSpec.feature "Potepan::Products", type: :feature do
  given(:taxonomy) { create(:taxonomy) }
  given(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  given(:product) { create(:product, taxons: [taxon]) }
  given!(:related_products) { create_list(:product, 4, taxons: [taxon]) }
  given!(:over_related_products) { create_list(:product, 5, taxons: [taxon]) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario 'タイトルにproduct.nameが含まれること' do
    expect(page).to have_title product.name
  end

  scenario 'ヘッダーに商品名とHOMEリンクがあること' do
    within '.pageHeader' do
      expect(page).to have_selector 'h2', text: product.name
      expect(page).to have_selector 'li', text: product.name
      expect(page).to have_link 'HOME'
      click_on 'HOME'
      expect(current_path).to eq potepan_index_path
    end
  end

  scenario '商品詳細ページに必要なリンクとテキストがあること' do
    within '.singleProduct' do
      expect(page).to have_selector 'h2', text: product.name
      expect(page).to have_selector 'h3', text: product.display_price.to_s
      expect(page).to have_selector 'p', text: product.description
      expect(page).to have_link '一覧ページへ戻る'
      expect(page).to have_link 'カートへ入れる'
    end
  end

  scenario '「一覧ページへ戻る」をクリックすると該当カテゴリーページに戻ること' do
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  scenario '関連商品が正しく表示されていること' do
    within '.productsContent' do
      related_products.each do |related_product|
        expect(page).to have_selector 'h5', text: related_product.name
        expect(page).to have_selector 'h3', text: related_product.display_price.to_s
        expect(page).to have_link related_product.name
        expect(page).to have_link related_product.display_price.to_s
        click_on related_product.name
        expect(current_path).to eq potepan_product_path(related_product.id)
      end
    end
  end

  scenario '関連商品が定数以上ではないこと' do
    within '.productsContent' do
      over_related_products.each do |related_product|
        expect(page).to have_selector '.productBox', count: 4
      end
    end
  end
end
