require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  given(:taxonomy) { create(:taxonomy) }
  given(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  given!(:product) { create(:product, taxons: [taxon]) }
  given!(:bag) { create(:taxon, name: 'bag', taxonomy: taxonomy) }
  given!(:mug)   { create(:taxon, name: 'mug', taxonomy: taxonomy) }
  given!(:shirt)   { create(:taxon, name: 'shirts', taxonomy: taxonomy) }
  given!(:tshirt) { create(:taxon, name: 'T-shirts', taxonomy: taxonomy) }
  given!(:producbag) { create(:product, name: 'bag', price: 10, taxons: [bag]) }
  given!(:productmug) { create(:product, name: 'mug', price: 20, taxons: [mug]) }
  given!(:productshirt) { create(:product, name: 'shirt', price: 30, taxons: [shirt]) }
  given!(:producttshirt) { create(:product, name: 'T-shirts', price: 40, taxons: [tshirt]) }

  background do
    visit potepan_category_path(taxon.id)
  end

  scenario 'サイドバーが正しく機能していること' do
    within '.sideBar' do
      click_on taxonomy.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  scenario '商品をクリックすると詳細ページへ飛び、「一覧ページへ戻る」をクリックすると該当カテゴリーページに戻ること' do
    click_on "#{product.name}-img"
    expect(current_path).to eq potepan_product_path(product.id)
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)

    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)

    click_on product.display_price
    expect(current_path).to eq potepan_product_path(product.id)
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  scenario '該当カテゴリーページを正しく表示すること' do
    visit potepan_category_path(bag.id)
    expect(page).to have_title bag.name
    within '.productBox' do
      expect(page).to have_content producbag.name
      expect(page).to have_content producbag.display_price
    end
    within '.pageHeader' do
      expect(page).to have_selector 'h2', text: bag.name
      expect(page).to have_selector 'li', text: bag.name
      expect(page).to have_link 'HOME'
      expect(page).to have_link 'SHOP'
      click_on 'HOME'
      expect(current_path).to eq potepan_index_path
    end
  end

  scenario 'カテゴリーに属さないproductがレスポンスに含まれていないこと' do
    visit potepan_category_path(bag.id)
    expect(page).to have_title bag.name
    within '.productBox' do
      expect(page).not_to have_content productmug.name
      expect(page).not_to have_content productshirt.name
      expect(page).not_to have_content producttshirt.name
      expect(page).not_to have_content productmug.display_price
      expect(page).not_to have_content productshirt.display_price
      expect(page).not_to have_content producttshirt.display_price
    end
    within '.pageHeader' do
      expect(page).not_to have_selector 'h2', text: mug.name
      expect(page).not_to have_selector 'li', text: mug.name
      expect(page).not_to have_selector 'h2', text: shirt.name
      expect(page).not_to have_selector 'li', text: shirt.name
      expect(page).not_to have_selector 'h2', text: tshirt.name
      expect(page).not_to have_selector 'li', text: tshirt.name
    end
  end
end
