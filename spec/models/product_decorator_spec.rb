require 'rails_helper'

RSpec.describe Spree::ProductDecorator, type: :model do
  describe '関連商品テスト' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    let(:product) { create(:product, name: "product", taxons: [taxon]) }
    let(:unrelated_product) { create(:product, taxons: [taxon]) }

    it "詳細ページの商品が関連商品には含まれないこと" do
      expect(product.related_products).not_to include product
    end

    it '関連しない商品は含まれないこと' do
      expect(product.related_products).not_to include unrelated_product.taxons
    end
  end
end
