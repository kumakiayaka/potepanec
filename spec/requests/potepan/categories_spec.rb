require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "カテゴリーページにて" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_category_path(taxon.id)
    end

    it "アクセスに成功すること" do
      expect(response).to have_http_status(:success)
    end

    it '200レスポンスが返ってくること' do
      expect(response).to have_http_status(200)
    end

    it "taxon名が表示されていること" do
      expect(response.body).to include taxon.name
    end

    it "product名が表示されていること" do
      expect(response.body).to include product.name
    end

    it "商品価格が表示されていること" do
      expect(response.body).to include product.price.to_s
    end

    it "各カテゴリーの商品数が表示されていること" do
      expect(response.body).to include taxon.products.count.to_s
    end
  end
end
