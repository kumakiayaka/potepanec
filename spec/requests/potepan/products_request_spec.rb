require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  describe "商品詳細ページ内で" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }

    before do
      get potepan_product_path product.id
    end

    it "正常にレスポンスを返すこと" do
      expect(response).to be_success
    end

    it "200レスポンスを返すこと" do
      expect(response).to have_http_status "200"
    end

    it "商品名が表示されていること" do
      expect(response.body).to include product.name
    end

    it "商品説明が表示されていること" do
      expect(response.body).to include product.description
    end

    it "商品価格が表示されていること" do
      expect(response.body).to include product.display_price.to_s
    end
  end
end
