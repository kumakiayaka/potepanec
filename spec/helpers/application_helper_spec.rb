require 'rails_helper'
RSpec.describe ApplicationHelper, type: :helper do
  describe 'ページ毎タイトル' do
    subject { full_title(page_title) }

    context '引数がある時' do
      let(:page_title) { 'test' }

      it { is_expected.to eq "test - #{ApplicationHelper::BASE_TITLE}" }
    end

    context '引数がnilの時' do
      let(:page_title) { nil }

      it { is_expected.to eq "くまき課題:BIGBAG Store" }
    end

    context '引数が空白の時' do
      let(:page_title) { "" }

      it { is_expected.to eq "くまき課題:BIGBAG Store" }
    end
  end
end
